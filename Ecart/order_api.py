from Ecart.models import Orders
from rest_framework import viewsets,permissions
from Ecart.serializers import OrderSerializer
from rest_framework.permissions import  IsAuthenticated
# from rest_framework.authentication import TokenAuthentication
from knox.auth import TokenAuthentication

class OrdersViewSet(viewsets.ModelViewSet):
    queryset = Orders.objects.all()
    permission_classes = [
        IsAuthenticated
    ]
    authentication_classes = [TokenAuthentication]
    serializer_class = OrderSerializer

