from django.contrib import admin
from Ecart.models import Products,Orders,OrderItems

# Register your models here.

admin.site.register(Products)
admin.site.register(Orders)
admin.site.register(OrderItems)

